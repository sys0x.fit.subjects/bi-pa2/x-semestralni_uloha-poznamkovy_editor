SOURCES=src/Data/Data.cpp src/Data/DataTypes/ChildDataTypes/ShoppingData.cpp src/Data/DataTypes/ChildDataTypes/TaskData.cpp src/Data/DataTypes/ChildDataTypes/TextData.cpp src/Data/DataTypes/DataType.cpp src/Data/Encryption/Encryption.cpp src/GUI/Editor/ChildEditorTypes/PlainText.cpp src/GUI/Editor/ChildEditorTypes/ShoppingList.cpp src/GUI/Editor/ChildEditorTypes/TaskList.cpp src/GUI/Editor/Editor.cpp src/GUI/Menu/LoadFileMenu.cpp src/GUI/Menu/MemoryMenu.cpp src/GUI/Menu/Menu.cpp src/GUI/Menu/NewFileMenu.cpp src/main.cpp
HEADERS=src/Data/Data.h src/Data/DataTypes/ChildDataTypes/ShoppingData.h src/Data/DataTypes/ChildDataTypes/TaskData.h src/Data/DataTypes/ChildDataTypes/TextData.h src/Data/DataTypes/DataType.h src/Data/Encryption/Encryption.h src/GUI/Editor/ChildEditorTypes/PlainText.h src/GUI/Editor/ChildEditorTypes/ShoppingList.h src/GUI/Editor/ChildEditorTypes/TaskList.h src/GUI/Editor/Editor.h src/GUI/Menu/LoadFileMenu.h src/GUI/Menu/MemoryMenu.h src/GUI/Menu/Menu.h src/GUI/Menu/NewFileMenu.h

OBJECTS=$(SOURCES:%.cpp=%.o)

TARGET=PoznamkovyEditor
COMPILER=g++
LIBS= -lncurses -lform -lmenu

all: compile doc

run: compile
	./$(TARGET)
	
compile: $(TARGET)

doc: $(SOURCES) $(HEADERS)
	doxygen doxygenConfigFile.txt

$(TARGET): $(OBJECTS)
	$(COMPILER) $^ -o 	$@ $(LIBS)

clean:
	rm -f $(TARGET) $(OBJECTS)
	rm -fr doc


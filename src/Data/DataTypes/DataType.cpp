//
// Created by malyp on 5/8/2019.
//

#include <iostream>
#include <vector>
#include "DataType.h"
#include <sstream>

vector<string> DataType::chopTextToLines(const string &text) {
    vector<string> chopText;

    stringstream stream(text);
    string line;

    while (getline(stream, line, '\n')) {
        chopText.push_back(line);
    }


    return chopText;
}

string DataType::getRidOfFirstLine(const string &rawData) {
    string data = rawData;

    data.erase(0, rawData.find('\n') + 1);//todo no hardcoding

    return data;
}

const string DataType::PLAIN = "#@PLAINTEXT@#";
const string DataType::SHOP = "#@SHOPPINGLIST@#";
const string DataType::TASK = "#@TASKLIST@#";


DataType::Type DataType::getFileType() {
    return _dataType;
}

string DataType::getFilePath() {
    return _filePath;
}

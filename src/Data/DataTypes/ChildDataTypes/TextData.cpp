//
// Created by malyp on 5/8/2019.
//

#include <iostream>
#include "TextData.h"

TextData::TextData(const string &rawData, const string &filePath) {

    _text = getRidOfFirstLine(rawData);
    _filePath = filePath;
    _dataType = PLAINTEXT;

    for (auto &rawChar : _text) {
        if (rawChar == '\n') {//todo no hardcoding
            _numberOfLines++;
        } else if (rawChar == '\r') {//todo no hardcoding
            _numberOfLines++;
        }
    }


}

int TextData::getNumberOfPositions() {
    return 1;
}

long long int TextData::getPrice(int position) {
    return 0;
}

bool TextData::getStatus(int position) {
    return false;
}


string TextData::getSaveFormat() {
    string firstLineText = DataType::PLAIN + '\n';//todo no hardcoding
    firstLineText.append(_text);
    return firstLineText;

}


string TextData::getText(int position) {
    return _text;
}

void TextData::replaceText(int position, const string &text) {
    _text = text;
}







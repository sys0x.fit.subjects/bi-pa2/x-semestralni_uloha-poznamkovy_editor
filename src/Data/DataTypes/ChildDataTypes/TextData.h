//
// Created by malyp on 5/8/2019.
//

#ifndef SEMESTRALNIULOHA_TEXTDATA_H
#define SEMESTRALNIULOHA_TEXTDATA_H


#include <vector>
#include "../DataType.h"
/// Class that stores data from plain text
/**
 * This class stores the data in a string that holds the whole document
 * */
class TextData : public DataType {
private:
    string _text;
    int _numberOfLines;

    long long int getPrice(int position) override;

    bool getStatus(int position) override;

public:
    /**constructor initializes  numberOfLines in text and formats and stores rawData in memory
     *
     * @param rawData
     * @param filePath
     */

    TextData(const string &rawData, const string &filePath);

    string getSaveFormat() override;

    string getText(int position) override;

    void replaceText(int position, const string &text);

    int getNumberOfPositions() override;


protected:


};


#endif //SEMESTRALNIULOHA_TEXTDATA_H

//
// Created by malyp on 5/10/2019.
//

#include <form.h>
#include <regex>
#include "ShoppingData.h"


string ShoppingData::getText(int position) {
    return _shoppingList[position].first;

}


void ShoppingData::setTextPrice(FIELD **_textFields, int _numberOfLines) {
    vector<pair<string, int>> data;
    data.reserve(_numberOfLines);
    for (int i = 0; i < _numberOfLines * 2; i += 2) {
        int price;
        try {
            price = stoi(regex_replace(field_buffer(_textFields[i + 1], 0), regex("^ +| +$|( ) +"), "$1"), nullptr, 10);
        } catch (...) {
            price = 0;
        }
        data.emplace_back(regex_replace(field_buffer(_textFields[i], 0), regex("^ +| +$|( ) +"), "$1"),
                          price);
    }
    _shoppingList = data;

}


string ShoppingData::getSaveFormat() {
    string output = SHOP + '\n';

    for (auto &i : _shoppingList) {
        output += i.first + '\n';
        output += to_string(i.second) + '\n';
    }
    return output;

}


ShoppingData::ShoppingData(const string &rawData, const string &filePath) {
    _dataType = SHOPPINGLIST;
    _filePath = filePath;

    string text = getRidOfFirstLine(rawData);
    vector<string> chText = chopTextToLines(text);

    for (int i = 0; i < chText.size(); i += 2) {
        if (i + 1 < chText.size()) {
            _shoppingList.emplace_back(chText[i], stoi(chText[i + 1], nullptr, 10));
        }
    }

}

int ShoppingData::getNumberOfPositions() {
    return _shoppingList.size();
}

long long int ShoppingData::getPrice(int position) {
    return _shoppingList[position].second;
}

bool ShoppingData::getStatus(int position) {
    return false;
}


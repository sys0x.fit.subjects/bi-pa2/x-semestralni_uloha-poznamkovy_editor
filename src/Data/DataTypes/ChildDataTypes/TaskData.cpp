//
// Created by malyp on 5/8/2019.
//

#include "TaskData.h"
#include "../../../GUI/Editor/ChildEditorTypes/TaskList.h"
#include <form.h>
#include <regex>


TaskData::TaskData(const string &rawData, const string &filePath) {

    _dataType = TASKLIST;
    _filePath = filePath;

    string text = getRidOfFirstLine(rawData);
    vector<string> chText = chopTextToLines(text);

    for (int i = 0; i < chText.size(); i += 2) {
        if (i + 1 < chText.size()) {
            _taskData.emplace_back(chText[i], stoi(chText[i + 1], nullptr, 10));
        }
    }


}

int TaskData::getNumberOfPositions() {
    return _taskData.size();
}

void TaskData::setTextStatus(FIELD **_textFields, int _numberOfLines) {
    vector<pair<string, bool>> data;
    data.reserve(_numberOfLines);
    for (int i = 0; i < _numberOfLines * 2; i += 2) {
        string status;
        bool stat;
        status = regex_replace(field_buffer(_textFields[i + 1], 0), regex("^ +| +$|( ) +"), "$1");

        stat = status == "COMPLETED";//todo make take TaskList::COMPLETED

        data.emplace_back(regex_replace(field_buffer(_textFields[i], 0), regex("^ +| +$|( ) +"), "$1"),
                          stat);
    }
    _taskData = data;

}

bool TaskData::getStatus(int position) {
    return _taskData[position].second;
}

string TaskData::getText(int position) {
    return _taskData[position].first;
}

long long int TaskData::getPrice(int position) {
    return 0;
}


string TaskData::getSaveFormat() {
    string output = TASK + '\n';

    for (auto &i : _taskData) {
        output += i.first + '\n';
        output += to_string(i.second) + '\n';
    }
    return output;

}


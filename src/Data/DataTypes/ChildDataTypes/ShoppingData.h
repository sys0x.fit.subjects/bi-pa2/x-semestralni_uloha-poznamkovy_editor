//
// Created by malyp on 5/10/2019.
//

#ifndef SEMESTRALNIULOHA_SHOPPINGDATA_H
#define SEMESTRALNIULOHA_SHOPPINGDATA_H

#include "../DataType.h"
#include <map>
#include <vector>

using namespace std;
/// Class that stores data from Shopping Lists
/**
 * This class stores the data in a vector<pair<string,int>>
 * where the string portion of the pair stores the data about what to object in the shopping list is
 * and the int portion stores the price of that item
 * */
class ShoppingData : public DataType {
private:
    vector<pair<string, int>> _shoppingList;

    bool getStatus(int position) override;

protected:


public:
    void setTextPrice(FIELD **_textFields, int _numberOfLines);

    long long int getPrice(int position) override;

    string getText(int position) override;


    string getSaveFormat() override;

    /**constructor initializes filePath and formats and stores rawData in memory (vector)
     *
     * @param rawData
     * @param filePath
     */
    ShoppingData(const string &rawData, const string &filePath);

    int getNumberOfPositions() override;

protected:


};


#endif //SEMESTRALNIULOHA_SHOPPINGDATA_H

//
// Created by malyp on 5/8/2019.
//

#ifndef SEMESTRALNIULOHA_TASKDATA_H
#define SEMESTRALNIULOHA_TASKDATA_H


#include "../DataType.h"
#include <form.h>

using namespace std;

/// Class that stores data from Task Lists
/**
 * This class stores the data in a vector<pair<string,bool>>
 * where the string portion of the pair stores the data about the task
 * and the bool portion stores if the task was completed
 * */
class TaskData : public DataType {
private:
    vector<pair<string, bool>> _taskData;

    long long int getPrice(int position) override;

protected:


public:
    void setTextStatus(FIELD **_textFields, int _numberOfLines);

    /**constructor initializes filePath and formats and stores rawData in memory (vector)
     *
     * @param rawData
     * @param filePath
     */
    TaskData(const string &rawData, const string &filePath);


    bool getStatus(int position) override;

    string getText(int position) override;

    string getSaveFormat() override;

    int getNumberOfPositions() override;


};


#endif //SEMESTRALNIULOHA_TASKDATA_H

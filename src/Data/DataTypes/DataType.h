//
// Created by malyp on 5/8/2019.
//

#ifndef SEMESTRALNIULOHA_DATATYPE_H
#define SEMESTRALNIULOHA_DATATYPE_H

#include <string>
#include <vector>

using namespace std;
/// Parent class for polymorfism
/**
 * This class is an abstract class that enables polymorfism for the child DataTypes
 * The job of DataTypes is to store the information about a file in memory.
 * each child DataType stores the content a bit differently for the purpose of adding logic to data
 * */
class DataType {
public:
    /**
     * Types that the aplication supports
     */
    enum Type {
        PLAINTEXT, SHOPPINGLIST, TASKLIST
    };
    /**
     * heders for file formats
     */
    static const string PLAIN, SHOP, TASK;
protected:
    string _filePath;
    int numberOfPositions = 0;

    /**
     *
     * @param rawData string to remove first line
     * @return returns modified string
     */
    string getRidOfFirstLine(const string &rawData);

    /**
     * stores information about type of data
     */
    Type _dataType;


    /**
     * divides string to lines
     * @param text  text to be decomposed
     * @return decomposed text
     */
    vector<string> chopTextToLines(const string &text);


public:
    /**
     * returns status if a task has been completed
     *
     * @param position
     * @return
     */
    virtual bool getStatus(int position) = 0;

    /**rewrites data in memory
     *
     * @param textPrice data to be stores
     *
     */

    virtual long long int getPrice(int position) = 0;

    /**
     * returns type of data stored
     * @return
     */

    DataType::Type getFileType();

    /**
     * returns the location where the data are stored
     * @return
     */
    virtual string getFilePath();

    /**
     * returns how many objects are stored in memory
     * @return
     */
    virtual int getNumberOfPositions() = 0;

    /**
     * returns the text of object
     * @param position of object
     * @return string with text
     */
    virtual string getText(int position) = 0;

    /**replaces text in the defined position
     *
     * @param position of text
     * @param text that will replace the old text
     *
     */
    virtual string getSaveFormat() = 0;

};


#endif //SEMESTRALNIULOHA_DATATYPE_H

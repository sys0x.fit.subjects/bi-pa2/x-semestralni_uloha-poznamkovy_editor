//
// Created by malyp on 5/8/2019.
//

#ifndef SEMESTRALNIULOHA_DATA_H
#define SEMESTRALNIULOHA_DATA_H

#include <vector>
#include "DataTypes/DataType.h"
#include <fstream>

using namespace std;
/// Class that holds all DataTypes in one place
/**
 * Class Data holds DataTypes in a vector it can add or create DataTypes,
 * It handles loading and saving the data in DataTypes to a file
 * */
class Data {
private:
    /**
     * vector that stores DataTypes
     */
    vector<DataType *> _data;

    /**gets raw data from file
     *
     * @param filePath path to file
     * @return string with raw data
     */
    string getRawData(const string &filePath);

    /**
     * opens output stream to file
     *
     * @param filePath path to file
     * @return
     */
    ofstream getOutputStream(const string &filePath);

public:
    /**
     * loads data into memory
     *
     * @param filePath path to file
     * @return returns TRUE if succeeded else FALSE
     */
    bool loadData(const string &filePath);

    /**
     * creates a new file
     *
     * @param type type of file to be created
     * @param filePath path to file
     * @return returns TRUE if succeeded else FALSE
     */
    bool newData(DataType::Type type, const string &filePath);

    /**
     * returns all the files stored in memory
     * @return
     */
    vector<string> getFileNames();

    /**
     * saves data in memory to folder
     *
     * @param position of file in memory
     * @param filePath path to file
     * @return returns TRUE if succeeded else FALSE
     */
    bool saveData(int position, const string &filePath);

    /**returns the data stored in the position of memory
     *
     * @param page position in memory
     * @return data of one file
     */
    DataType *getDataType(int page);

    /**
     * destructor cleans the allocated from datatypes
     */

    ~Data();

};


#endif //SEMESTRALNIULOHA_DATA_H

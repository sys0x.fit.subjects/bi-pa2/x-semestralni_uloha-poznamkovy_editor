//
// Created by malyp on 5/10/2019.
//

#ifndef SEMESTRALNIULOHA_ENCRYPTION_H
#define SEMESTRALNIULOHA_ENCRYPTION_H

#include <string>

using namespace std;

/// Class that encrypts decrypts text
class Encryption {

    /**encrypts string
     *
     * @param rawData string to be encrypted
     * @return encrypted string
     */
    const string &encrypt(const string &rawData);

    /**decrypts string
     *
     * @param rawData string to be decrypted
     * @return decrypted string
     */

    const string &decrypt(const string &rawData);
};


#endif //SEMESTRALNIULOHA_ENCRYPTION_H

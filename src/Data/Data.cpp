//
// Created by malyp on 5/8/2019.
//

#include <iostream>
#include <curses.h>
#include "Data.h"
#include "DataTypes/ChildDataTypes/TaskData.h"
#include "DataTypes/ChildDataTypes/ShoppingData.h"
#include "DataTypes/ChildDataTypes/TextData.h"

using namespace std;


string Data::getRawData(const string &filePath) {
    ifstream inFileStream;
    inFileStream.open(filePath);
    string rawData;

    if (!inFileStream.good()) {
        return string();
    }
    string tmp;
    while (!inFileStream.eof()) {

        rawData += inFileStream.get();

    }

    return rawData;
}

ofstream Data::getOutputStream(const string &filePath) {
    ofstream outputStream;
    outputStream.open(filePath, ofstream::out | ofstream::trunc);

    if (outputStream.good()) {
        return outputStream;
    }
    return std::ofstream();
}

bool Data::loadData(const string &filePath) {

    string rawData = getRawData(filePath);

    string firstRow = rawData.substr(0, rawData.find('\n'));
    firstRow = firstRow.substr(0, rawData.find('\r'));

    if (rawData.empty()) {
        return false;
    }

    if (firstRow == DataType::PLAIN) {
        _data.push_back(new TextData(rawData, filePath));
    } else if (firstRow == DataType::SHOP) {
        _data.push_back(new ShoppingData(rawData, filePath));
    } else if (firstRow == DataType::TASK) {
        _data.push_back(new TaskData(rawData, filePath));
    } else {
        _data.push_back(new TextData(DataType::PLAIN + "\n" + rawData, filePath));
    }

    return true;
}


bool Data::saveData(int position, const string &filePath) {


    ofstream outputStream = getOutputStream(_data[position]->getFilePath());
    if (!outputStream.good()) {
        return false;//todo prompt user that file was not saved
    }
    outputStream << _data[position]->getSaveFormat();//todo put encription option here

    if (!outputStream.good()) {
        return false;//todo prompt user that file was not fully saved
    }
    outputStream.close();

    return true;
}

DataType *Data::getDataType(int page) {
    if (_data.empty() || page >= _data.size() || page < 0) {
        return nullptr;//todo check if it works
    }
    return _data[page];
}

Data::~Data() {
    if (!_data.empty()) {
        for (auto &i : _data) {
            delete i;//todo fix
        }
    }


}

vector<string> Data::getFileNames() {
    vector<string> fileNames;

    fileNames.reserve(_data.size());//prealocating data
    for (auto &i : _data) {
        fileNames.push_back(i->getFilePath());
    }

    return fileNames;
}

bool Data::newData(DataType::Type type, const string &filePath) {

    ofstream outfile = getOutputStream(filePath);

    if (!outfile.good()) {
        return false;
    }
    switch (type) {
        case DataType::SHOPPINGLIST: {
            outfile << DataType::SHOP << endl;
            break;
        }
        case DataType::TASKLIST: {
            outfile << DataType::TASK << endl;
            break;
        }
        case DataType::PLAINTEXT: {
            outfile << DataType::PLAIN << endl;
            break;
        }
    }
    if (!outfile.good()) {
        return false;
    }
    outfile.close();
    return this->loadData(filePath);

}




//
// Created by malyp on 5/10/2019.
//

#include <regex>
#include "TaskList.h"


TaskList::TaskList(int numberOfLines, const string &fileName) {
    _fileName = fileName;
    _numberOfLines = numberOfLines;
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    initAll();

}

void TaskList::initTextFields() {
    //delete *_textFields;//todo check if it works
    _textFields = new FIELD *[_numberOfLines * 2 + 1];

    for (int i = 0, y = 0; i < _numberOfLines; i++, y += 2) {

        _textFields[y] = new_field(1, getmaxx(_subMainWin) / 2 - 1, i, 0, 0, 0);
        _textFields[y + 1] = new_field(1, getmaxx(_subMainWin) / 2 - 1, i, getmaxx(_subMainWin) / 2 + 1, 0, 0);


        //todo add data to _textFields and add

        set_field_back(_textFields[y], A_UNDERLINE);    /* Print a line for the option 	*/
        field_opts_off(_textFields[y], O_AUTOSKIP);/* Don't go to next field when this Field is filled up 		*/
        field_opts_off(_textFields[y], O_STATIC);//make field grow infinetly

        set_field_back(_textFields[y + 1], A_UNDERLINE);    /* Print a line for the option 	*/
        field_opts_off(_textFields[y + 1], O_AUTOSKIP);/* Don't go to next field when this Field is filled up 		*/
        field_opts_off(_textFields[y + 1], O_STATIC);

    }
    _textFields[_numberOfLines * 2] = NULL;

}

void TaskList::load() {
    for (int i = 0, y = 0; i < _dataType->getNumberOfPositions() * 2; i += 2, y++) {
        set_field_buffer(_textFields[i], 0, _dataType->getText(y).c_str());
        if (_dataType->getStatus(y)) {
            set_field_buffer(_textFields[i + 1], 0, COMPLETED.c_str());
        } else {
            set_field_buffer(_textFields[i + 1], 0, NCOMPLETED.c_str());
        }

    }
    for (int i = _dataType->getNumberOfPositions() * 2, y = 0; i < _numberOfLines * 2; i += 2, y++) {
        set_field_buffer(_textFields[i], 0, "");
        set_field_buffer(_textFields[i + 1], 0, NCOMPLETED.c_str());
    }
}

void TaskList::initTopInfoText() {
    mvwprintw(_subTopInfoWin, 0, 0, _fileName.c_str());
}

void TaskList::save() {
    _dataType->setTextStatus(_textFields, _numberOfLines);

}

void TaskList::printLines() {
    for (int i = 0; i < _numberOfLines; i++) {
        mvwprintw(_subLeftInfoWin, i, 0, "NO.:\t");
        wprintw(_subLeftInfoWin, to_string(i).c_str());
    }
}

void TaskList::initBottomRightInfoText() {
    if (_textFields != nullptr) {
        werase(_subBottomRightInfoWin);
        mvwprintw(_subBottomRightInfoWin, 0, 0, "|  Number Tasks:");
        wprintw(_subBottomRightInfoWin, to_string(_numberOfLines).c_str());
        wprintw(_subBottomRightInfoWin, "  |  Completion:");
        wprintw(_subBottomRightInfoWin, to_string(getTotalComplete()).c_str());
        wprintw(_subBottomRightInfoWin, " %");
    }

}

void TaskList::initForms() {
    _form = new_form(_textFields);
}

void TaskList::combine() {
    set_form_win(_form, _mainWin);
    set_form_sub(_form, _subMainWin);
    post_form(_form);

}

int TaskList::start() {
    printLines();
    refreshAll();
    int ch;
    while (ch = getch()) {
        switch (ch) {
            case KEY_BACKSPACE:
                form_driver(_form, REQ_DEL_PREV);
                break;
            case KEY_LEFT:
                form_driver(_form, REQ_PREV_CHAR);
                break;

            case KEY_RIGHT:
                form_driver(_form, REQ_NEXT_CHAR);
                break;
            case KEY_DOWN:
                form_driver(_form, REQ_NEXT_FIELD);
                break;
            case KEY_UP:
                form_driver(_form, REQ_PREV_FIELD);
                break;
            case KEY_F(6): {
                if (_numberOfLines > 0) {
                    clear();
                    TaskList addedList(_numberOfLines - 1, _fileName);
                    addedList._dataType = _dataType;
                    deletItem(current_field(_form)->index);
                    addedList.loadTextFields(_textFields);
                    return addedList.start();
                } else {
                    break;

                }
            }
            case KEY_F(5): {
                clear();
                TaskList addedList(_numberOfLines + 1, _fileName);
                addedList._dataType = _dataType;
                addedList.loadTextFields(_textFields);
                return addedList.start();
            }
            case KEY_F(2)://discard
                exit();
                return 0;
            case KEY_F(1)://save
                save();
                werase(_subBottomLeftInfoWin);
                mvwprintw(_subBottomLeftInfoWin, 0, 0, "Saving press any key to continue");
                refreshAll();
                getch();
                exit();
                return 1;
            case 10: {
                form_driver(_form, REQ_END_FIELD);
                form_driver(_form, REQ_NEXT_FIELD);
                break;
            }
            default:
                if ((current_field(_form)->index) % 2 == 0) {
                    form_driver(_form, ch);
                    if (ch != 32) {//spacebar
                        form_driver(_form, REQ_END_FIELD);
                    }
                } else {
                    changeFace(_form);
                }
                break;
        }

        refreshAll();
    }

    return 0;
}

void TaskList::initBottomLeftInfoText() {
    werase(_subBottomLeftInfoWin);
    mvwprintw(_subBottomLeftInfoWin, 0, 0, "|  F1: SAVE  |  F2: DISCARD | F5: ADD | F6: REMOVE");

}

TaskList::TaskList(const string &fileName, DataType *dataType) : TaskList(dataType->getNumberOfPositions(), fileName) {
    _dataType = dynamic_cast<TaskData *>(dataType);
    load();
}

void TaskList::changeFace(FORM *driver) {
    FIELD *tempField = current_field(driver);

    string str = field_buffer(tempField, 0);

    for (int y = 0; y < str.size(); y++) {
        if (str[y] == ' ') {
            str = str.substr(0, y);
        }
    }
    if (str == "COMPLETED") {
        set_field_buffer(tempField, 0, NCOMPLETED.c_str());

    } else {
        set_field_buffer(tempField, 0, COMPLETED.c_str());
    }


}

long long int TaskList::getTotalComplete() {
    double completed = 0;
    for (int i = 1; i < _numberOfLines * 2; i += 2) {

        string str = field_buffer(_textFields[i], 0);

        for (int y = 0; y < str.size(); y++) {
            if (str[y] == ' ') {
                str = str.substr(0, y);
            }
        }
        if (str == COMPLETED) {
            completed++;

        }
    }


    if (completed == 0) {
        return 0;
    }
    return completed / _numberOfLines * 100;
}

void TaskList::loadTextFields(FIELD **textField) {
    int i = 0;
    while (textField[i] != NULL) {
        set_field_buffer(_textFields[i], 0, field_buffer(textField[i], 0));
        set_field_buffer(_textFields[i + 1], 0, field_buffer(textField[i + 1], 0));
        i += 2;

    }

    for (; i < _numberOfLines * 2; i += 2) {
        set_field_buffer(_textFields[i], 0, "");
        set_field_buffer(_textFields[i + 1], 0, NCOMPLETED.c_str());
    }


}

TaskList::~TaskList() {
    delete[] _textFields;
}



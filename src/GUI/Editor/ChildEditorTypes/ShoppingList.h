//
// Created by malyp on 5/10/2019.
//

#ifndef SEMESTRALNIULOHA_SHOPPINGLIST_H
#define SEMESTRALNIULOHA_SHOPPINGLIST_H


#include "../Editor.h"
#include "../../../Data/DataTypes/ChildDataTypes/ShoppingData.h"

/// This child class creates the main editing window for shopping list

class ShoppingList : public Editor {
public:
    ShoppingData *_dataType;
    /**
     * contructor that initilazes empty file with number of lines
     * @param numberOfLines to be shown
     * @param fileName name of file
     */
    explicit ShoppingList(int numberOfLines, const string &fileName);

    /**
     * constuctor that initilazes empty file
     * and loads data into it
     * @param fileName
     * @param dataType data to be loaded
     */
    ShoppingList(const string &fileName, DataType *dataType);

    int start() override;

protected:
    void load() override;

    /**
     * returns the total price of goods in shopping list
     * @return
     */

    long long int getTotalPrice();

    void save() override;

    void initTextFields() override;

    /**
     * loads text into textfields from textFields
     * @param textField textFielsds to load into
     */
    void loadTextFields(FIELD **textField);

    void initForms() override;

    void combine() override;

    /**
     * print lines in left window
     */
    void printLines();

    void initTopInfoText() override;

    void initBottomRightInfoText() override;

    void initBottomLeftInfoText() override;


};


#endif //SEMESTRALNIULOHA_SHOPPINGLIST_H

//
// Created by malyp on 5/10/2019.
//

#include <regex>
#include <algorithm>
#include <iostream>
#include "ShoppingList.h"


ShoppingList::ShoppingList(int numberOfLines, const string &fileName) {
    _fileName = fileName;
    _numberOfLines = numberOfLines;
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    initAll();

}

void ShoppingList::initTextFields() {
    //delete *_textFields;//todo check if it works
    _textFields = new FIELD *[_numberOfLines * 2 + 1];

    for (int i = 0, y = 0; i < _numberOfLines; i++, y += 2) {

        _textFields[y] = new_field(1, getmaxx(_subMainWin) / 2 - 1, i, 0, 0, 0);
        _textFields[y + 1] = new_field(1, getmaxx(_subMainWin) / 2 - 1, i, getmaxx(_subMainWin) / 2 + 1, 0, 0);


        //todo add data to _textFields and add

        set_field_back(_textFields[y], A_UNDERLINE);    /* Print a line for the option 	*/
        field_opts_off(_textFields[y], O_AUTOSKIP);/* Don't go to next field when this Field is filled up 		*/
        field_opts_off(_textFields[y], O_STATIC);//make field grow infinetly

        set_field_type(_textFields[y + 1], TYPE_INTEGER, 1);
        set_field_back(_textFields[y + 1], A_UNDERLINE);    /* Print a line for the option 	*/
        field_opts_off(_textFields[y + 1], O_AUTOSKIP);/* Don't go to next field when this Field is filled up 		*/
        field_opts_off(_textFields[y + 1], O_STATIC);

    }
    _textFields[_numberOfLines * 2] = NULL;

}

void ShoppingList::load() {
    for (int i = 0, y = 0; i < _dataType->getNumberOfPositions() * 2; i += 2, y++) {
        set_field_buffer(_textFields[i], 0, _dataType->getText(y).c_str());
        set_field_buffer(_textFields[i + 1], 0, to_string(_dataType->getPrice(y)).c_str());

    }
    for (int i = _dataType->getNumberOfPositions() * 2, y = 0; i < _numberOfLines * 2; i += 2, y++) {
        set_field_buffer(_textFields[i], 0, "");
        set_field_buffer(_textFields[i + 1], 0, "0");
    }
}

void ShoppingList::initTopInfoText() {
    mvwprintw(_subTopInfoWin, 0, 0, _fileName.c_str());
}


void ShoppingList::save() {
    _dataType->setTextPrice(_textFields, _numberOfLines);

}

void ShoppingList::printLines() {
    for (int i = 0; i < _numberOfLines; i++) {
        mvwprintw(_subLeftInfoWin, i, 0, "NO.:\t");
        wprintw(_subLeftInfoWin, to_string(i).c_str());
    }
}

void ShoppingList::initBottomRightInfoText() {
    if (_textFields != nullptr) {
        werase(_subBottomRightInfoWin);
        mvwprintw(_subBottomRightInfoWin, 0, 0, "|  Number of lines:");
        wprintw(_subBottomRightInfoWin, to_string(_numberOfLines).c_str());
        wprintw(_subBottomRightInfoWin, "  |  Total price:");
        wprintw(_subBottomRightInfoWin, to_string(getTotalPrice()).c_str());
    }

}

void ShoppingList::initForms() {
    _form = new_form(_textFields);
}

void ShoppingList::combine() {
    set_form_win(_form, _mainWin);
    set_form_sub(_form, _subMainWin);
    post_form(_form);

}

int ShoppingList::start() {
    printLines();
    refreshAll();
    int ch;
    while (ch = getch()) {
        switch (ch) {
            case KEY_BACKSPACE:
                form_driver(_form, REQ_DEL_PREV);
                break;
            case KEY_LEFT:
                form_driver(_form, REQ_PREV_CHAR);
                break;

            case KEY_RIGHT:
                form_driver(_form, REQ_NEXT_CHAR);
                break;
            case KEY_DOWN:
                form_driver(_form, REQ_NEXT_FIELD);
                break;
            case KEY_UP:
                form_driver(_form, REQ_PREV_FIELD);
                //form_driver(_form, REQ_END_LINE);
                break;
            case KEY_F(6): {
                if (_numberOfLines > 0) {
                    clear();
                    ShoppingList addedList(_numberOfLines - 1, _fileName);
                    addedList._dataType = _dataType;
                    deletItem(current_field(_form)->index);
                    addedList.loadTextFields(_textFields);
                    return addedList.start();
                } else {
                    break;

                }
            }
            case KEY_F(5): {

                clear();
                ShoppingList addedList(_numberOfLines + 1, _fileName);
                addedList._dataType = _dataType;
                addedList.loadTextFields(_textFields);
                return addedList.start();
            }
            case KEY_F(2)://discard
                exit();
                return 0;
            case KEY_F(1)://save
                save();
                werase(_subBottomLeftInfoWin);
                mvwprintw(_subBottomLeftInfoWin, 0, 0, "Saving press any key to continue");
                refreshAll();
                getch();
                exit();
                return 1;
            case 10: {
                form_driver(_form, REQ_END_FIELD);
                form_driver(_form, REQ_NEXT_FIELD);
                break;
            }
            default:
                form_driver(_form, ch);
                if (ch != 32) {//spacebar
                    form_driver(_form, REQ_END_FIELD);
                }
                break;
        }

        refreshAll();
    }

    return 0;
}

void ShoppingList::initBottomLeftInfoText() {
    werase(_subBottomLeftInfoWin);
    mvwprintw(_subBottomLeftInfoWin, 0, 0, "|  F1: SAVE  |  F2: DISCARD | F5: ADD | F6: REMOVE");
}

ShoppingList::ShoppingList(const string &fileName, DataType *dataType) : ShoppingList(dataType->getNumberOfPositions(),
                                                                                      fileName) {
    _dataType = dynamic_cast<ShoppingData *>(dataType);
    load();
}

long long int ShoppingList::getTotalPrice() {
    long long int totalPrice = 0;
    for (int i = 1; i < _numberOfLines * 2; i += 2) {

        string str = field_buffer(_textFields[i], 0);

        for (int y = 0; y < str.size(); y++) {
            if (str[y] == ' ') {
                str = str.substr(0, y);
            }
        }
        try {
            totalPrice += stoi(str, nullptr, 10);
        } catch (...) {
            return -1;
        }
    }

    return totalPrice;
}

void ShoppingList::loadTextFields(FIELD **textField) {
    for (int i = 0, y = 0; i < (_numberOfLines - 1) * 2; i += 2, y++) {
        set_field_buffer(_textFields[i], 0, field_buffer(textField[i], 0));
        set_field_buffer(_textFields[i + 1], 0, field_buffer(textField[i + 1], 0));

    }
    for (int i = (_numberOfLines - 1) * 2; i < _numberOfLines * 2; i += 2) {
        set_field_buffer(_textFields[i], 0, "");
        set_field_buffer(_textFields[i + 1], 0, "0");
    }


}





//
// Created by malyp on 5/10/2019.
//

#include <regex>
#include <iostream>
#include "PlainText.h"


PlainText::PlainText(const string &fileName) {
    _fileName = fileName;
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);

    initAll();

}

void PlainText::initTextFields() {

    _textFields = new FIELD *[2];
    _textFields[0] = new_field(getmaxy(_subMainWin), getmaxx(_subMainWin), 0, 0, 0, 0);

    field_opts_off(_textFields[0], O_AUTOSKIP);/* Don't go to next field when this Field is filled up 		*/
    field_opts_off(_textFields[0], O_STATIC);

    _textFields[1] = NULL;


}


void PlainText::initForms() {
    _form = new_form(_textFields);
}

void PlainText::combine() {
    set_form_win(_form, _mainWin);
    set_form_sub(_form, _subMainWin);
    post_form(_form);

}

int PlainText::start() {
    clear();

    refreshAll();

    int ch;
    while (true) {

        ch = getch();

        switch (ch) {
            case KEY_BACKSPACE:
                form_driver(_form, REQ_DEL_PREV);

                break;
            case KEY_LEFT:
                form_driver(_form, REQ_PREV_CHAR);
                break;

            case KEY_RIGHT:
                form_driver(_form, REQ_NEXT_CHAR);
                break;
            case KEY_DOWN:
                form_driver(_form, REQ_DOWN_CHAR);
                break;
            case KEY_UP:
                form_driver(_form, REQ_UP_CHAR);

                break;
            case KEY_F(2)://discard
                exit();
                return 0;
            case KEY_F(1)://save
                save();
                werase(_subBottomLeftInfoWin);
                mvwprintw(_subBottomLeftInfoWin, 0, 0, "Saving press any key to continue");
                refreshAll();
                getch();
                exit();
                return 1;
            case 10: {
                form_driver(_form, REQ_NEW_LINE);
                break;
            }
            default:
                form_driver(_form, ch);
                _numberOfChars++;
                break;
        }

        refreshAll();
    }

}

void PlainText::load() {

    set_field_buffer(_textFields[0], 0, _dataType->getText(0).c_str());
}

void PlainText::save() {
    _dataType->replaceText(0, field_buffer(_textFields[0], 0));

}

PlainText::PlainText(string fileName, DataType *dataType) : PlainText(fileName) {
    _dataType = dynamic_cast<TextData *>(dataType);
    load();
}

PlainText::~PlainText() {
    exit();
}

void PlainText::initTopInfoText() {
    mvwprintw(_subTopInfoWin, 0, 0, _fileName.c_str());
}




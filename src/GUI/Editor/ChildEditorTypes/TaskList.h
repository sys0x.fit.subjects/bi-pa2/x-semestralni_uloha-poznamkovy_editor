//
// Created by malyp on 5/10/2019.
//

#ifndef SEMESTRALNIULOHA_TASKLIST_H
#define SEMESTRALNIULOHA_TASKLIST_H

#include "../Editor.h"
#include "../../../Data/DataTypes/ChildDataTypes/TaskData.h"
/// This child class creates the main editing window for Task list

class TaskList : public Editor {
private:

public:
    TaskData *_dataType;
    /**
     * contructor that initilazes empty file with number of lines
     * @param numberOfLines to be shown
     * @param fileName name of file
     */
    TaskList(int numberOfLines, const string &fileName);

    /**
     * constuctor that initilazes empty file
     * and loads data into it
     * @param fileName
     * @param dataType data to be loaded
     */
    TaskList(const string &fileName, DataType *dataType);

    int start() override;

    ~TaskList();

    string COMPLETED = "COMPLETED", NCOMPLETED = "NOT COMPLETED";

protected:
    void combine() override;

    void initForms() override;

    void initTextFields() override;

    void load() override;

    void save() override;

    void initTopInfoText() override;


    /**
     * loads text into textfields from textFields
     * @param textField textFielsds to load into
     */
    void loadTextFields(FIELD **textField);

    /**
     * print lines in left window
     */
    void printLines();

    void initBottomRightInfoText() override;

    void initBottomLeftInfoText() override;

    /**
     * returns the total completion rate of tasks
     * @return int from 0-100
     */
    long long int getTotalComplete();

    /**
     * changes the field from COMPLETE to NOT COMPLETE and from NOT COMPLETE to COMPLETE
     * @param driver where to save
     */
    void changeFace(FORM *driver);


};


#endif //SEMESTRALNIULOHA_TASKLIST_H

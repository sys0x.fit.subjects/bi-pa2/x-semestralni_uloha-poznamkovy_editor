//
// Created by malyp on 5/10/2019.
//

#ifndef SEMESTRALNIULOHA_PLAINTEXT_H
#define SEMESTRALNIULOHA_PLAINTEXT_H

#include "../Editor.h"
#include "../../../Data/DataTypes/ChildDataTypes/TextData.h"

/// This child class creates the main editing window for plain text


class PlainText : public Editor {
public:
    TextData *_dataType;
    /**
     * constuctor that initilazes empty file
     * and loads data into it
     * @param fileName
     * @param dataType data to be loaded
     */
    PlainText(string fileName, DataType *dataType);

    /**
     * contructor that initilazes empty file
     * @param fileName name of file
     */

    explicit PlainText(const string &fileName);

    int start() override;

    ~PlainText();

protected:
    void initTextFields() override;

    void initForms() override;

    void combine() override;

    void load() override;

    void save() override;


    void initTopInfoText() override;
};


#endif //SEMESTRALNIULOHA_PLAINTEXT_H

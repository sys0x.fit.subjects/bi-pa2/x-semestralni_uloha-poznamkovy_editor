//
// Created by malyp on 5/10/2019.
//

#ifndef SEMESTRALNIULOHA_EDITOR_H
#define SEMESTRALNIULOHA_EDITOR_H


#include <curses.h>
#include <form.h>
#include "../../Data/DataTypes/DataType.h"
#include "../../Data/Data.h"
/// This abstract class is used to create the default template for the child classes
/**
 * This class initilaze the Windows and dictates what the child classes have to implemente mainly the start function
 *
 * */
class Editor {
protected:

    WINDOW *_mainWin;
    WINDOW *_subMainWin;

    WINDOW *_bottomLeftInfoWin;
    WINDOW *_subBottomLeftInfoWin;

    WINDOW *_bottomRightInfoWin;
    WINDOW *_subBottomRightInfoWin;

    WINDOW *_topInfoWin;
    WINDOW *_subTopInfoWin;

    WINDOW *_leftInfoWin;
    WINDOW *_subLeftInfoWin;

    FIELD **_textFields = nullptr;
    FORM *_form;
    /**
     * data in memory
     */

    /**
     * name of file
     */
    string _fileName;
    /**
     * number of lines in text and number of chars in text
     */
    int _numberOfLines, _numberOfChars;
    /**
     * first displayed row
     */
    int _topRow = 0;


    /**
     * deletes line
     * @param index on what line to delete
     */
    void deletItem(short index);

    /**
     * x screen size
     */
    int _xScreenSize;
    /**
     * y screen size
     */
    int _yScreenSize;

    /**
     * adds text to bottom left window
     */
    virtual void initBottomLeftInfoText();

    /**
     * adds text to bottom right window
     */
    virtual void initBottomRightInfoText();

    /**
     * initializes Windows
     */
    virtual void initWins();

    /**
     * dictates in what order will things be initialized
     */
    virtual void initAll();

    /**
     * refreshes screen
     */
    virtual void refreshAll();

    /**
     * dealocs memory
     */
    virtual void exit();

    /**
    * combines forms and windows
    *
    */
    virtual void combine() = 0;

    /**
     * initializes Forms
     */
    virtual void initForms() = 0;

    /**
     * initializes text fields (FIELD)
     */
    virtual void initTextFields() = 0;

    /**
     * loads data from memory
     */
    virtual void load() = 0;

    /**
     * saves data to memory
     */
    virtual void save() = 0;

    /**
     * draws boxes around windows
     */
    virtual void drawBoxes();

    /**
     * adds text into top windows
     */
    virtual void initTopInfoText() = 0;

public:
    /**
     * starts the menu for the user
     * @return reurns if to save folder 1 for folders to be saved else does not want to be saved
     *
     */
    virtual int start() = 0;


};


#endif //SEMESTRALNIULOHA_EDITOR_H

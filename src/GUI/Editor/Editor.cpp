//
// Created by malyp on 5/10/2019.
//

#include "Editor.h"

void Editor::initAll() {
    getmaxyx(stdscr, _yScreenSize, _xScreenSize);
    initWins();
    drawBoxes();

    initBottomLeftInfoText();
    initBottomRightInfoText();
    initTopInfoText();

    initTextFields();

    initForms();

    //add more init things

    combine();
}

void Editor::initWins() {
    int numberOfCharsLeftCol = 15;

    _topInfoWin = newwin(3, getmaxx(stdscr), 0, 0);
    _subTopInfoWin = derwin(_topInfoWin, 1, getmaxx(_topInfoWin) - 2, 1, 1);

    _leftInfoWin = newwin(getmaxy(stdscr) - getmaxy(_topInfoWin) - 3, numberOfCharsLeftCol + 2, getmaxy(_topInfoWin),
                          0);
    _subLeftInfoWin = derwin(_leftInfoWin, getmaxy(_leftInfoWin) - 2, numberOfCharsLeftCol, 1, 1);

    _mainWin = newpad(getmaxy(_leftInfoWin), getmaxx(stdscr) - getmaxx(_leftInfoWin));
    _subMainWin = subpad(_mainWin, getmaxy(_mainWin) - 2, getmaxx(_mainWin) - 2, 1, 1);

    _bottomLeftInfoWin = newwin(getmaxy(stdscr) - getmaxy(_leftInfoWin) - getmaxy(_topInfoWin), getmaxx(stdscr) / 2,
                                getmaxy(_leftInfoWin) + getmaxy(_topInfoWin), 0);
    _subBottomLeftInfoWin = derwin(_bottomLeftInfoWin, 1, getmaxx(_bottomLeftInfoWin) - 2, 1, 1);

    _bottomRightInfoWin = newwin(getmaxy(stdscr) - getmaxy(_leftInfoWin) - getmaxy(_topInfoWin), getmaxx(stdscr) / 2,
                                 getmaxy(_leftInfoWin) + getmaxy(_topInfoWin), getmaxx(stdscr) / 2);
    _subBottomRightInfoWin = derwin(_bottomRightInfoWin, 1, getmaxx(_bottomRightInfoWin) - 2, 1, 1);


}

void Editor::drawBoxes() {
    box(_topInfoWin, 0, 0);
    box(_leftInfoWin, 0, 0);
    box(_mainWin, 0, 0);
    box(_bottomLeftInfoWin, 0, 0);
    box(_bottomRightInfoWin, 0, 0);
}

void Editor::initBottomLeftInfoText() {
    werase(_subBottomLeftInfoWin);
    mvwprintw(_subBottomLeftInfoWin, 0, 0, "|  F1: SAVE AND EXIT  |  F2: DISCARD AND EXIT  |");
}

void Editor::initBottomRightInfoText() {

}


void Editor::refreshAll() {
    initBottomRightInfoText();

    refresh();

    wrefresh(_topInfoWin);
    touchwin(_topInfoWin);
    wrefresh(_subTopInfoWin);

    wrefresh(_leftInfoWin);
    touchwin(_leftInfoWin);
    wrefresh(_subLeftInfoWin);

    wrefresh(_bottomLeftInfoWin);
    touchwin(_bottomLeftInfoWin);
    wrefresh(_subBottomLeftInfoWin);

    wrefresh(_bottomRightInfoWin);
    touchwin(_bottomRightInfoWin);
    wrefresh(_subBottomRightInfoWin);

    prefresh(_mainWin, _topRow, 0, getmaxy(_topInfoWin), getmaxx(_leftInfoWin), getbegy(_bottomLeftInfoWin),
             getmaxx(stdscr));

}

void Editor::exit() {
    endwin();
}

void Editor::deletItem(short index) {

    for (index; index < _numberOfLines * 2 - 2; index += 2) {
        _textFields[index] = _textFields[index + 2];
        _textFields[index + 1] = _textFields[index + 3];
    }
    _textFields[index] = NULL;
    _textFields[index + 1] = NULL;

}



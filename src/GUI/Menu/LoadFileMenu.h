//
// Created by malyp on 5/23/2019.
//

#ifndef SEMESTRALNIULOHA_LOADFILEMENU_H
#define SEMESTRALNIULOHA_LOADFILEMENU_H

/// This class is used to get the location of the file to be loaded/ created
/**
 * This classes main job is to get the file location from the user.
 *
 * */

class LoadFileMenu : public Menu {
public:
    /**
     * starts the menu for the user
     * @return info about completion
     */
    string start() override;

    /**
     * constructor creates the windows,forms and inits everything
     * @param data
     */
    LoadFileMenu();

private:

    FIELD *_field[2];
    FORM *_form;

    void initAll() override;

    void refreshAll() override;


    void combine(int numberOfItemsToApperInMenu) override;

    /**
     * initializes Form
     */
    void initForm();


};


#endif //SEMESTRALNIULOHA_LOADFILEMENU_H

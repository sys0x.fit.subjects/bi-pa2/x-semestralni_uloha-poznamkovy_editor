//
// Created by malyp on 5/9/2019.
//

#ifndef SEMESTRALNIULOHA_MENU_H
#define SEMESTRALNIULOHA_MENU_H

#include <form.h>
#include <curses.h>
#include <menu.h>
#include <array>
#include "../../Data/Data.h"


using namespace std;
/// Main menu; The parrent class for all the menus
/**
 * This class is the main menu for the program it is the first class to call on start and the last to close
 *
 *
 * */
class Menu {
protected:
    WINDOW *_menuWin;
    WINDOW *_subMenuWin;
    MENU *_menu;
    ITEM **_items;
    Data *_data;
    int _xScreenSize;
    int _yScreenSize;
    /**
     * items to be displayed
     */
    vector<string> _menuItems = {"Load file", "New File", "Open File From Memory", "Exit"};

    /**
     * initializes menu
     */
    void initMen();

    /**
     * initializes Windows
     */
    void initWin();

    /**
     * dictates in what order will things be initialized
     */
    virtual void initAll();

    /**
     * combines menus and windows
     * @param number Of Items To Apper In Menu
     */
    virtual void combine(int numberOfItemsToApperInMenu);

    /**
     * refreshes the screen
     */
    virtual void refreshAll();

    /**
     * dealocates memory
     */

public:
    /**
     * starts the menu for the user
     * @return info about completion
     */
    virtual string start();

    /**
     * constructor creates the windows and inits everything
     */
    Menu();

    ~Menu();


};


#endif //SEMESTRALNIULOHA_MENU_H

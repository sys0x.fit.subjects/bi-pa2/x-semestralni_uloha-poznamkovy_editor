//
// Created by malyp on 5/28/2019.
//

#ifndef SEMESTRALNIULOHA_NEWFILEMENU_H
#define SEMESTRALNIULOHA_NEWFILEMENU_H

#include "Menu.h"
/// This class is used to create new files
/**
 * From this class you can create new file types and save them to memory
 *
 * */
class NewFileMenu : public Menu {
private:
    NewFileMenu();

public:
    /**
     * constructor creates the windows and inits everything also gets data that are in memory
     */

    explicit NewFileMenu(Data *data);

    /**
     * starts the menu for the user
     * @return info about completion
     */
    string start() override;


};


#endif //SEMESTRALNIULOHA_NEWFILEMENU_H

//
// Created by malyp on 5/28/2019.
//

#ifndef SEMESTRALNIULOHA_MEMORYMENU_H
#define SEMESTRALNIULOHA_MEMORYMENU_H


#include "Menu.h"
#include "../Editor/Editor.h"
/// This class is used to open files stored in memory
/**
 * This class displays the files that were created and loaded to memory
 * And from here you can open the files
 *
 * */

class MemoryMenu : public Menu {

public:
    /**
     * starts the menu for the user
     * @return info about completion
     */
    string start() override;

    /**
     * constructor creates the windows and inits everything also gets data that are in memory
     * @param data
     */
    MemoryMenu(Data *data);
};


#endif //SEMESTRALNIULOHA_MEMORYMENU_H

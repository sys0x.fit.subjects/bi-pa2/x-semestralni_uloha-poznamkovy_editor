//
// Created by malyp on 5/28/2019.
//

#include "MemoryMenu.h"
#include "../Editor/ChildEditorTypes/PlainText.h"
#include "../Editor/ChildEditorTypes/TaskList.h"
#include "../Editor/ChildEditorTypes/ShoppingList.h"

MemoryMenu::MemoryMenu(Data *data) {
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    getmaxyx(stdscr, _yScreenSize, _xScreenSize);
    _data = data;
    _menuItems = data->getFileNames();
    initAll();

}


string MemoryMenu::start() {
    refreshAll();
    int c;
    while ((c = getch()) != KEY_F(1)) {
        switch (c) {
            case KEY_DOWN:
                menu_driver(_menu, REQ_DOWN_ITEM);
                break;
            case KEY_UP:
                menu_driver(_menu, REQ_UP_ITEM);
                break;
            case 10: { //enter
                ITEM *cur_item;
                cur_item = current_item(_menu);
                DataType *data = _data->getDataType(cur_item->index); //todo finish

                switch (data->getFileType()) {//todo fix
                    case DataType::PLAINTEXT: {//plain text
                        PlainText t(_menuItems[cur_item->index], data);
                        clear();
                        if (t.start()) {
                            if (!_data->saveData(cur_item->index, data->getFilePath())) {
                                clear();
                                printw("NOT SAVED press any key to continue");
                                getch();
                            } else {
                                clear();
                                printw("SAVED press any key to continue");
                                getch();

                            }
                        }
                        clear();
                        break;
                    }
                    case DataType::TASKLIST: {//task list
                        TaskList t(_menuItems[cur_item->index], data);
                        clear();
                        if (t.start()) {
                            if (!_data->saveData(cur_item->index, data->getFilePath())) {
                                clear();
                                printw("NOT SAVED");
                                getch();
                            } else {
                                clear();
                                printw("SAVED press any key to continue");
                                getch();

                            }
                        }

                        clear();
                        break;
                    }
                    case DataType::SHOPPINGLIST: {//shopping list
                        ShoppingList t(_menuItems[cur_item->index], data);
                        clear();
                        if (t.start()) {
                            if (!(_data->saveData(cur_item->index, data->getFilePath()))) {
                                clear();
                                printw("NOT SAVED press any key to continue");
                                getch();
                            } else {
                                clear();
                                printw("SAVED press any key to continue");
                                getch();

                            }
                        }
                        clear();
                        break;
                    }
                    default: {//auto plain text
//                        PlainText t(_menuItems[cur_item->index],data);
                        clear();
//                        t.start();
                        clear();
                        break;
                    }
                }


            }
        }
        refreshAll();
    }
    return "END NOT OK";
}

//
// Created by malyp on 5/9/2019.
//

#include <iostream>
#include "Menu.h"
#include "LoadFileMenu.h"
#include "NewFileMenu.h"
#include "MemoryMenu.h"
#include <curses.h>


Menu::Menu() {
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    getmaxyx(stdscr, _yScreenSize, _xScreenSize);
    initAll();
    _data = new Data;

}

void Menu::initAll() {

    initMen();

    initWin();

    combine(10);//todo no hardcoding
}

void Menu::combine(int numberOfItemsToApperInMenu) {
    set_menu_win(_menu, _menuWin);
    set_menu_sub(_menu, _subMenuWin);
    set_menu_format(_menu, numberOfItemsToApperInMenu, 1);
    post_menu(_menu);

}

void Menu::initMen() {

    _items = new ITEM *[_menuItems.size() + 1];

    for (int i = 0; i < _menuItems.size(); i++) {
        _items[i] = new_item(_menuItems[i].c_str(), "");
    }

    _items[_menuItems.size()] = NULL;

    _menu = new_menu(_items);

}

void Menu::initWin() {
    _menuWin = newwin(15, _xScreenSize - (10), 5, 5);//todo no hardcoding
    _subMenuWin = subwin(_menuWin, getmaxy(_menuWin) - 5, getmaxx(_menuWin) - 10, getbegy(_menuWin) + 3,
                         getbegx(_menuWin) + 5);//todo no hardcoding
    box(_menuWin, 0, 0);
}

void Menu::refreshAll() {
    refresh();
    wrefresh(_menuWin);
    touchwin(_menuWin);
    wrefresh(_subMenuWin);
}

string Menu::start() {

    refreshAll();
    int c;
    while ((c = getch()) != KEY_F(1)) {
        switch (c) {
            case KEY_DOWN:
                menu_driver(_menu, REQ_DOWN_ITEM);
                break;
            case KEY_UP:
                menu_driver(_menu, REQ_UP_ITEM);
                break;
            case 10: { //enter
                ITEM *cur_item;
                cur_item = current_item(_menu);
                switch (cur_item->index) {
                    case 0: {//load file
                        clear();
                        LoadFileMenu loadFileMenu;
                        string str = loadFileMenu.start();
                        clear();
                        bool sucess = _data->loadData(str);//todo make it load multiple files at a time;

                        if (sucess) {
                            mvwprintw(_menuWin, 10, 4, "Loaded:   ");
                        } else {
                            mvwprintw(_menuWin, 10, 4, "NOT Loaded:   ");
                        }


                        wprintw(_menuWin, str.c_str());

                        break;

                    }
                    case 1: {//new file
                        clear();

                        NewFileMenu newFileMenu(_data);

                        newFileMenu.start();

                        clear();
                        break;
                    }
                    case 2: {//open from memory
                        MemoryMenu newMenu(_data);
                        clear();
                        newMenu.start();
                        clear();
                        break;
                    }
                    case 3: {//exit
                        delete _data;
                        return "END OK";
                    }

                    default:
                        break;

                }
            }


        }

        refreshAll();
    }
    return "END NOT OK";
}


Menu::~Menu() {

    for (int i = 0; _menu->items[i] != NULL; i++) {
        free_item(_menu->items[i]);
    }
    free_menu(_menu);
    delete[] _items;
    endwin();
}





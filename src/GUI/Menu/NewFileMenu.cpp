//
// Created by malyp on 5/28/2019.
//

#include "NewFileMenu.h"
#include "../Editor/ChildEditorTypes/PlainText.h"
#include "../Editor/ChildEditorTypes/ShoppingList.h"
#include "../Editor/ChildEditorTypes/TaskList.h"
#include "LoadFileMenu.h"

NewFileMenu::NewFileMenu() {

    _menuItems = {"Plain text", "Shopping List", "Task List", "Exit"};
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    getmaxyx(stdscr, _yScreenSize, _xScreenSize);

    initAll();


}

string NewFileMenu::start() {
    refreshAll();
    int c;
    while ((c = getch()) != KEY_F(1)) {
        switch (c) {
            case KEY_DOWN:
                menu_driver(_menu, REQ_DOWN_ITEM);
                break;
            case KEY_UP:
                menu_driver(_menu, REQ_UP_ITEM);
                break;
            case 10: { //enter
                ITEM *cur_item;
                cur_item = current_item(_menu);
                switch (cur_item->index) {
                    case 0: {//Plain text
                        clear();
                        LoadFileMenu loadFileMenu;
                        string filePath = loadFileMenu.start();
                        clear();
                        bool sucess = _data->newData(DataType::PLAINTEXT,
                                                     filePath);//todo make it load multiple files at a time;
                        clear();
                        if (sucess) {
                            printw("Loaded:   ");
                        } else {
                            printw("NOT Loaded:   ");
                        }

                        printw(filePath.c_str());
                        printw("   press any key to continue");
                        getch();
                        return "OK";

                    }
                    case 1: {//Shopping List
                        clear();
                        LoadFileMenu loadFileMenu;
                        string filePath = loadFileMenu.start();
                        clear();
                        bool sucess = _data->newData(DataType::SHOPPINGLIST,
                                                     filePath);//todo make it load multiple files at a time;
                        clear();
                        if (sucess) {
                            printw("Loaded:   ");
                        } else {
                            printw("NOT Loaded:   ");
                        }

                        printw(filePath.c_str());
                        printw("   press any key to continue");
                        getch();
                        return "OK";
                    }
                    case 2: {//Tast List
                        clear();
                        LoadFileMenu loadFileMenu;
                        string filePath = loadFileMenu.start();
                        clear();
                        bool sucess = _data->newData(DataType::TASKLIST,
                                                     filePath);//todo make it load multiple files at a time;
                        clear();
                        if (sucess) {
                            printw("Loaded:   ");
                        } else {
                            printw("NOT Loaded:   ");
                        }

                        printw(filePath.c_str());
                        printw("   press any key to continue");
                        getch();

                        return "OK";
                    }
                    default: {//exit
                        return "END OK";
                    }

                }
            }


        }

        refreshAll();
    }
    return "END NOT OK";
}


NewFileMenu::NewFileMenu(Data *data) : NewFileMenu() {
    _data = data;
}

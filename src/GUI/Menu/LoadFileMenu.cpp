//
// Created by malyp on 5/23/2019.
//
#include <curses.h>
#include <menu.h>
#include <form.h>
#include <regex>
#include "Menu.h"
#include "LoadFileMenu.h"

string LoadFileMenu::start() {
    refreshAll();
    int ch;
    while ((ch = getch()) != KEY_F(1)) {
        switch (ch) {
            case KEY_BACKSPACE:
                form_driver(_form, REQ_DEL_PREV);
                break;
            case KEY_LEFT:
                form_driver(_form, REQ_PREV_CHAR);
                break;

            case KEY_RIGHT:
                form_driver(_form, REQ_NEXT_CHAR);
                break;
            case 10: {

                form_driver(_form, REQ_END_LINE);
                return regex_replace(field_buffer(_field[0], 0), regex("^ +| +$|( ) +"),
                                     "$1");//todo figure out how to return field value
            }
            default:
                form_driver(_form, ch);
                break;
        }
        refreshAll();
    }

    return "OK";
}

void LoadFileMenu::initAll() {
    initWin();
    initForm();
    combine(5);//todo no hardcoding
}

void LoadFileMenu::initForm() {
/* Initialize the fields */
    mvwprintw(_menuWin, getpary(_subMenuWin) - 1, 1, "Path to file");
    _field[0] = new_field(1, getmaxx(_subMenuWin), 1, 0, 0, 0);//todo no hardcoding
    _field[1] = NULL;


    /* Set field options */
    set_field_back(_field[0], A_UNDERLINE);    /* Print a line for the option 	*/
    field_opts_off(_field[0], O_AUTOSKIP);/* Don't go to next field when this Field is filled up 		*/
    field_opts_off(_field[0], O_STATIC);//make field grow infinetly

    /* Create the form and post it */
    _form = new_form(_field);


}

void LoadFileMenu::refreshAll() {
    refresh();
    wrefresh(_menuWin);
    touchwin(_menuWin);
    wrefresh(_subMenuWin);
}


void LoadFileMenu::combine(int numberOfItemsToApperInMenu) {
//    set_menu_win(_menu, _menuWin);
//    set_menu_sub(_menu, _subMenuWin);
//    set_menu_format(_menu, numberOfItemsToApperInMenu, 1);
//    post_menu(_menu);
    set_form_win(_form, _menuWin);
    set_form_sub(_form, _subMenuWin);
    post_form(_form);
}

LoadFileMenu::LoadFileMenu() {
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    getmaxyx(stdscr, _yScreenSize, _xScreenSize);
    initAll();
}

